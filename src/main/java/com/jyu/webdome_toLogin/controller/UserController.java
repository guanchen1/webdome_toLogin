package com.jyu.webdome_toLogin.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jyu.webdome_toLogin.common.R;
import com.jyu.webdome_toLogin.entity.User;
import com.jyu.webdome_toLogin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;


    @PostMapping("/login")
    public R<User> login(@RequestBody User map, HttpSession session){
        System.out.println(map.toString());

        String username  = (String) map.getName();
        String password  = (String) map.getPhone();



        if ( username != null  ){
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getName,username);
            User user = userService.getOne(queryWrapper);

            if( user != null && user.getPhone() .equals(password)){
                return R.success(user);
            }
        }
        return R.error("登陆失败");
    }



    @GetMapping("/hello")
    public R<String> hello (){
        return  R.success("hello");
    }

}
