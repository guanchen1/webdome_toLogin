package com.jyu.webdome_toLogin.config;

import com.jyu.webdome_toLogin.common.JacksonObjectMapper;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

@Configuration
public class SpringmvcConfig extends WebMvcConfigurationSupport {

    /**
     * @description: 设置静态资源映射
     * @param registry
     * @return: void
     * @date: 2023/7/26 11:13
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {

        System.out.println("静态资源访问。。。");
        registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
        registry.addResourceHandler("/front/**").addResourceLocations("classpath:/front/");
    }

    
    /**
     * @description: 消息转换类，将 java 类型转换成 json格式，发给前端
     * @param converters 
     * @return: void
     * @date: 2023/7/26 23:11
     */
    @Override
    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {

        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();

        messageConverter.setObjectMapper(new JacksonObjectMapper());

        converters.add(0,messageConverter);
        super.extendMessageConverters(converters);
    }


}
