package com.jyu.webdome_toLogin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jyu.webdome_toLogin.entity.User;

public interface UserService extends IService<User> {
}
