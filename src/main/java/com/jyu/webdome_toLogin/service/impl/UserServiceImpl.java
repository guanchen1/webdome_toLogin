package com.jyu.webdome_toLogin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jyu.webdome_toLogin.entity.User;
import com.jyu.webdome_toLogin.mapper.UserMapper;
import com.jyu.webdome_toLogin.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
