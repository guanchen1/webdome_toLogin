package com.jyu.webdome_toLogin;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@Slf4j
@SpringBootApplication
@ServletComponentScan   //配置Servlet、Filter和Listener等组件后 启用对Servlet组件的扫描和注册。
        // Boot会自动扫描并注册带有@WebServlet、@WebFilter和@WebListener注解的组件
// @EnableTransactionManagement  // 启用Spring的事务管理功能。保证一组数据库操作的一致性和完整性。框架会自动为带有@Transactional注解的方法开启事务
// @EnableCaching //开启注解缓存功能
/*

http://localhost:8080/backend/page/login/index.html

*/

public class ReggieApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReggieApplication.class ,args);
        System.out.println("项目启动成功");
    }
}
