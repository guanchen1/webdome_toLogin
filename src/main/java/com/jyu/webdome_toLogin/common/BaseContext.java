package com.jyu.webdome_toLogin.common;


public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * @description: 在同一个线程内，可以设置一个 域存放变量 id
     * @param id 
     * @return: void
     * @date: 2023/7/28 17:31
     */
    public static void setCurrentId(Long id) {
        threadLocal.set(id);
    }

    public static Long getCurrentId() {
        return threadLocal.get();
    }
}
