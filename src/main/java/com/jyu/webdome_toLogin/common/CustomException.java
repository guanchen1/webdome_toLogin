package com.jyu.webdome_toLogin.common;

public class CustomException extends RuntimeException{
    /**
     * 之定义异常类对象
     * @param message
     */
    public CustomException(String message){
        super(message);
    }
}
