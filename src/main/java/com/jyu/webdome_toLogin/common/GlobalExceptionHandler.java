package com.jyu.webdome_toLogin.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLIntegrityConstraintViolationException;

@ControllerAdvice(annotations = {RestController.class, Controller.class})
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {


    /**
     * @description: 数据库异常捕获
     * @param ex
     * @return: com.jyu.reggie.common.R<java.lang.String>
     * @date: 2023/7/26 23:12
     */
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public  R<String> exceptionHandler (SQLIntegrityConstraintViolationException ex){
        log.info(ex.getMessage());

        if(ex.getMessage().contains("Duplicate entry")){
            String [] split = ex.getMessage().split(" ");
            String msg = split[2] + "已存在";
            return R.error(msg);
        }

        return  R.error("未知错误");
    }

    /**
     * @description:  自定义异常处理方法
     * @param ex 
     * @return: com.jyu.reggie.common.R<java.lang.String>
     * @date: 2023/7/27 8:40
     */
    @ExceptionHandler(CustomException.class)
    public  R<String> exceptionHandler (CustomException ex){
        log.info(ex.getMessage());
        return  R.error(ex.getMessage());
    }
}
