package com.jyu.webdome_toLogin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyu.webdome_toLogin.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
